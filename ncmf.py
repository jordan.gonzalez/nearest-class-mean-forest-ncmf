from __future__ import annotations
from abc import ABC, abstractmethod
import numpy as np
from scipy.stats import entropy
from itertools import combinations
import random
from scipy.spatial import distance
from math import inf
from collections import Counter
from sklearn.metrics import calinski_harabasz_score as calinski
from sklearn.metrics import confusion_matrix
import copy
from tqdm import tqdm
#from scipy.stats import multivariate_normal
from numpy.random import multivariate_normal

class AbstractNode(ABC):
    def __init__(self,*args,**kwargs):
        self._infos_node = {}
        self._left_child = None
        self._right_child = None
        self._is_leaf = False
        self._depth = 0
        self._distribution = None
    def set_leaf(self,leaf=True):
        self._is_leaf = leaf
    def is_leaf(self):
        return self._is_leaf
    def set_depth(self,n):
        self._depth = n
    def get_depth(self):
        return self._depth
    def get_left_child(self) -> AbstractNode:
        return self._left_child
    def get_right_child(self) -> AbstractNode:
        return self._right_child
    def set_left_child(self,node):
        self._left_child = node
    def set_right_child(self,node):
        self._right_child = node
    def get_infos(self):
        return self._infos_node
    def set_infos(self,infos):
        self._infos_node = infos
    def add_infos(self,infokey,infoval):
        self._infos_node[infokey] = infoval
    @abstractmethod
    def set_distribution(self,*args,**kwargs):
        pass
    def get_distribution(self):
        return self._distribution

class AbstractTree(ABC):
    def __init__(self,max_depth=None,*args,**kwargs):
        self._root = None
        self.max_depth = max_depth
        self._infos_tree = {}
    @abstractmethod
    def _init_root(self,*args,**kwargs) -> AbstractNode:
        pass
    def get_root(self):
        return self._root
    @abstractmethod
    def fit(self,X,y=None,*args,**kwargs):
        pass
    @abstractmethod
    def predict_proba(self,*args,**kwargs):
        pass
    @abstractmethod
    def predict(self,*args,**kwargs):
        pass
    @abstractmethod
    def _find_best_split(self,*args,**kwargs):
        pass
    @abstractmethod
    def _evaluate_split(self,*args,**kwargs):
        pass
    @abstractmethod
    def _get_direction(self,*args,**kwargs):
        pass
    @abstractmethod
    def score(self,X,y,*args,**kwargs):
        pass
    @abstractmethod
    def _build_tree(self,*args,**kwargs):
        pass
    def get_infos(self):
        return self._infos_tree
    def set_infos(self,infos):
        self._infos_tree = infos
    def add_infos(self,infokey,infoval):
        self._infos_tree[infokey] = infoval

class Classical_Tree(AbstractTree):
    def __init__(self,max_depth=None,min_sample_split=2,max_features=None,min_sample_leaf=1):
        super().__init__(max_depth)
        self.min_sample_split = min_sample_split
        self.min_sample_leaf = min_sample_leaf
        self.max_features = max_features
        self._classes = None
        self._maximum_depth_reached = 0
    def _set_classes(self,y):
        if y is not None:
            self._classes = np.unique(y)
    def fit(self, X, y=None, *args, **kwargs):
        self._set_classes(y)
        self._set_max_features(X)
        root = self._init_root(X,y)
        self._build_tree(root,X,y)
    def _set_max_features(self,X):
        #integer, float (proportion), sqrt, log2 or None
        if isinstance(self.max_features, int):
            assert self.max_features >= 1 and self.max_features <= X.shape[1], 'Dimensions out of bounds, {}'.format(self.max_features)
        elif isinstance(self.max_features, float):
            self.max_features = int(X.shape[1]*self.max_features)
            assert self.max_features >=1 and self.max_features <= X.shape[1], 'Dimensions out of bounds, {}'.format(self.max_features)
        elif isinstance(self.max_features, str):
            if self.max_features == 'sqrt':
                self.max_features = int(np.sqrt(X.shape[1]))
            elif self.max_features == 'log2':
                self.max_features = int(np.log2(X.shape[1]))
            else:
                raise ValueError('str not known: {}'.format(self.max_features))
            assert self.max_features >=1 and self.max_features <= X.shape[1], 'Dimensions out of bounds, {}'.format(self.max_features)
        elif self.max_features is None:
            self.max_features = X.shape[1]
    def score(self,X,y,measure='acc',decimals=3):
        predictions = self.predict(X)
        if measure == 'acc':
            return np.mean(predictions == y)
        raise NotImplementedError(f'Measure {measure} not implemented.')
    def _get_maximum_depth_reached(self):
        return self._maximum_depth_reached
    def _set_maximum_depth_reached(self,val):
        if val > self._maximum_depth_reached:
            self._maximum_depth_reached = val
    def prepare_leaf(self,node:AbstractNode,depth,y,*args,**kwargs):
        node.set_leaf()
        node.set_depth(depth)
        self._set_maximum_depth_reached(depth)
        node.set_distribution(y,self._classes)
    def prepare_node(self,node:AbstractNode,depth,features_indexes,infos_split,*args,**kwargs):
        node.add_infos('split',infos_split)
        node.set_depth(depth)
        node.add_infos('features_indexes', features_indexes)
    def is_pure(self,y):
        # Pure if samples in y share the same label
        return 1 == len(np.unique(y))
    def _entropy_score(self, y):
        _, counts = np.unique(y, return_counts=True)
        distrib = counts/len(y)
        score = entropy(distrib, base=2)
        return score
    def _entropy_split(self, y0, y1, y2):
        '''
        Expected entropy
        works for quantitative feature
        y0 = class distribution (c.d.) in parent
        y1 = c.d. in left child
        y2 = c.d. in right child
        '''
        n_y0 = float(len(y0))
        return (len(y1)/n_y0)*self._entropy_score(y1)+(len(y2)/n_y0)*self._entropy_score(y2)
    def information_gain(self, yparent, yleftchild,yrightchild,*args):
        '''
        compute information gain 
        Receives class distributions
        y{i} = class distrib in node
        '''
        score = self._entropy_score(yparent)
        return score - self._entropy_split(yparent, yleftchild,yrightchild, *args)

class NCM_Node_v0(AbstractNode):
    def get_id_nearest_centroid(self,sample,centroids,metric):
        dists = distance.cdist(sample, centroids, metric)
        min_cents = np.argmin(dists,axis=1)
        return min_cents
    def get_centroids(self,X,y,labels):
        centroids = []
        for label in labels:
            idxs_label = y == label
            centroids.append(X[idxs_label].mean(axis=0))
        return np.array(centroids)
    def set_distribution(self, y, classes_,*args, **kwargs):
        classes,counts=np.unique(y,return_counts=True)
        self._distribution = (classes,counts)


class NCM_Tree_v0(Classical_Tree):
    def __init__(self,max_depth=None,min_sample_split=2,max_features=None,min_sample_leaf=1,max_classes=None,*args,**kwargs):
        super().__init__(max_depth,min_sample_split,max_features,min_sample_leaf, *args,**kwargs)
        self.max_classes = max_classes

    def is_stopping_condition_reached(self,y,current_depth):
        val_return = False
        if self.max_depth is not None:
            if current_depth >= self.max_depth:
                val_return =  True
        if self.is_pure(y):
            val_return =  True
        return val_return

    def prepare_node(self,node:AbstractNode,depth,features_indexes,infos_split,*args,**kwargs):
        super().prepare_node(node,depth,features_indexes,infos_split)
        node.set_left_child(NCM_Node_v0())
        node.set_right_child(NCM_Node_v0())
        node.get_left_child().add_infos('parent',node)
        node.get_right_child().add_infos('parent',node)

    def _build_tree(self, node:AbstractNode,X,y,current_depth=0):
        if self.is_stopping_condition_reached(y,current_depth):
            self.prepare_leaf(node,current_depth,y)
        else:
            features_indexes = sorted(random.sample(range(X.shape[1]), self.max_features))
            left,right,is_split,infos_split = self._find_best_split(node,X[:,features_indexes],y)
            if is_split:
                self.prepare_node(node,current_depth, features_indexes, infos_split)
                self._build_tree(node.get_left_child(),X[left],y[left],current_depth+1)
                self._build_tree(node.get_right_child(),X[right],y[right],current_depth+1)
            else:
                # if cannot split then make a leaf
                self.prepare_leaf(node,current_depth,y)
    

    def _evaluate_split(self,yp,yl,yr,*args, **kwargs):
        if len(yp) == 0 or len(yl) == 0 or len(yr) == 0:
            return -inf
        return self.information_gain(yp,yl,yr)

    def class_bagging(self,y):
        if (self.max_classes is not None) and (self.max_classes <= len(np.unique(y))):
            subk = random.sample(list(np.unique(y)),self.max_classes)
        else:
            subk = list(np.unique(y))
        return subk

    def get_centroids(self,node,X,y,idxs_pair,labels_pair):
        return node.get_centroids(X[idxs_pair],y[idxs_pair],labels_pair)

    def get_nearest_centroid_label(self,node,X,centroids,metric='euclidean'):
        '''
        Has to return 0 or 1
        '''
        idxs = node.get_id_nearest_centroid(X,centroids,metric)
        return idxs

    def _find_best_split(self,node,X,y):
        # Best split is the one which maximizes _evaluate_split() function.
        classes_node_to_consider = self.class_bagging(y)
        # Get all pairwise combinations of classes_node_to_consider
        combs = list(combinations(classes_node_to_consider, 2))
        measures = []
        idxs_left_all = []
        idxs_right_all = []
        centroids_all = []
        for comb in combs:
            idxs_pair = np.isin(y,comb)
            centroids = self.get_centroids(node,X,y,idxs_pair,comb)
            centroids_all.append(centroids)
            idxs_cent = self.get_nearest_centroid_label(node,X,centroids,'euclidean')
            left_idxs = idxs_cent == 0#comb[0]
            right_idxs = idxs_cent == 1#comb[1]
            idxs_left_all.append(left_idxs)
            idxs_right_all.append(right_idxs)
            measure = self._evaluate_split(y, y[left_idxs],y[right_idxs])
            measures.append(measure)
        best_comb_id = np.argmax(measures)
        if measures[best_comb_id] == (-inf):
            return None,None,False,None
        infos_split = {
            'centroids':centroids_all[best_comb_id],
            'pair_labels':combs[best_comb_id],
            'infogain':measures[best_comb_id],
            'classbagg':classes_node_to_consider
        }
        return idxs_left_all[best_comb_id],idxs_right_all[best_comb_id],True,infos_split

    def _init_root(self, *args, **kwargs) -> AbstractNode:
        self._root = NCM_Node_v0()
        return self._root
    
    def _get_direction(self, sample, node:NCM_Node_v0, metric='euclidean', *args, **kwargs):
        direction = None
        centroids = node.get_infos()['split']['centroids']
        id_nearest_cent = self.get_nearest_centroid_label(node,sample[:,node.get_infos()['features_indexes']],centroids,metric)[0]
        if id_nearest_cent == 0:
            direction = 'left'
        elif id_nearest_cent == 1:
            direction = 'right'
        if direction is None:
            raise ValueError(f"direction is not found")
        return direction
    
    def get_max_class(self,probas_sample):
        return self._classes[np.argmax(probas_sample)]

    def predict(self, X, mode='majority_vote',*args, **kwargs):
        if 'phi' in kwargs:
            probas = self.predict_proba(X, mode = kwargs['phi'])
        else:
            probas = self.predict_proba(X)
        if mode == 'majority_vote':
            return np.array(list(map(self.get_max_class,probas)))
        else:
            raise NotImplementedError(f"'{mode}' mode is not implemented")
    
    def _propagate_to_leaf(self,sample,node:AbstractNode,metric='euclidean')->AbstractNode:
        if node.is_leaf():
            return node
        else:
            direction = self._get_direction(sample,node,metric)
            if direction == 'left':
                return self._propagate_to_leaf(sample,node.get_left_child())
            elif direction == 'right':
                return self._propagate_to_leaf(sample,node.get_right_child())
            else:
                raise ValueError(f"direction must be 'left' or 'right'. Found {direction}")

    def predict_proba(self, X, mode='probas', metric='euclidean', *args, **kwargs):
        res = []
        for x in X:
            leaf = self._propagate_to_leaf(x.reshape(1,-1),self.get_root())
            classes,counts = leaf.get_distribution()
            probas = []
            i=0
            for k in self._classes:
                if k in classes:
                    probas.append(counts[i])
                    i+=1
                else:
                    probas.append(0)
            probas = np.array(probas)
            if mode == 'probas':
                res.append(probas/counts.sum())
            elif mode == 'counts':
                res.append(probas)
            else:
                raise NotImplementedError(f"mode '{mode}' is not implemented.")
        return np.array(res)

class NCM_Tree_Incremental_v0(NCM_Tree_v0):
    def fit(self, X, y=None, indexes_train = None,current_depth=None,info_direction=None,*args, **kwargs):
        #self._set_classes(y)
        self._set_max_features(X)
        root = self._init_root(X,y)
        if indexes_train is None:
            indexes_train = np.array([i for i in range(len(X))])
        if current_depth is None:
            current_depth = 0
        self._build_tree(root,X,y,indexes_train=indexes_train, current_depth=current_depth,info_direction=info_direction)
    # modify prepare_leaffunction in order to store indexes of train set samples 
    def prepare_leaf(self,node:AbstractNode,depth,y,*args,**kwargs):
        super().prepare_leaf(node,depth,y)
        node.add_infos('idxs_train',kwargs['indexes_train'])
    # add indexes of X everywhere ...
    def _build_tree(self, node:AbstractNode,X,y,current_depth=0,indexes_train=None,*args,**kwargs):
        if 'info_direction' in kwargs and current_depth != 0:
            node.add_infos('position_relative_to_parent',kwargs['info_direction'])
        if self.is_stopping_condition_reached(y,current_depth):
            self.prepare_leaf(node,current_depth,y,indexes_train=indexes_train)
        else:
            features_indexes = sorted(random.sample(range(X.shape[1]), self.max_features))
            left,right,is_split,infos_split = self._find_best_split(node,X[:,features_indexes],y)
            if is_split:
                self.prepare_node(node,current_depth, features_indexes, infos_split)
                self._build_tree(node.get_left_child(),X[left],y[left],current_depth+1,indexes_train[left],info_direction='left')
                self._build_tree(node.get_right_child(),X[right],y[right],current_depth+1,indexes_train[right],info_direction='right')
            else:
                # if cannot split then make a leaf
                self.prepare_leaf(node,current_depth,y,indexes_train=indexes_train)

    # concatenate Xnew and Xold (do the same with y arrays)
    # observation : a node can receive multiple samples so this is better than incrementing sample by sample
    def IGT(self,Xnew,ynew,Xold,yold):
        _classesnew = np.unique(ynew)
        for k in _classesnew:
            if k not in self._classes:
                self._classes = np.concatenate([self._classes,np.array([k])])
        leaves = []
        idxs_samples_reaching_specific_leaves = {}
        adapted_idxs_samples_reaching_specific_leaves = {}
        for i,x in enumerate(Xnew):
            leaf = self._propagate_to_leaf(x.reshape(1,-1), self.get_root(), metric='euclidean')
            if leaf not in leaves:
                idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                adapted_idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                leaves.append(leaf)
            idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i)
            adapted_idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i+len(Xold))
        
        
        
        for leaf_index,indexes_samples in idxs_samples_reaching_specific_leaves.items():

            idxs_train = leaves[int(leaf_index.split('_')[1])].get_infos()['idxs_train']

            data_concat = np.concatenate([Xold[idxs_train,:], Xnew[indexes_samples,:]], axis=0)
            assert data_concat.shape[0] == (Xold[idxs_train,:].shape[0] + Xnew[indexes_samples,:].shape[0])
            assert data_concat.shape[1] == Xold.shape[1] == Xnew.shape[1]

            target_concat = np.concatenate([yold[idxs_train], ynew[indexes_samples]])
            assert len(target_concat) == (len(yold[idxs_train]) + len(ynew[indexes_samples]))


            tree = NCM_Tree_Incremental_v0(
                max_features=self.max_features,max_classes=self.max_classes,
                max_depth=self.max_depth,min_sample_leaf=self.min_sample_leaf,
                min_sample_split=self.min_sample_split
                )
            idxs_all = np.concatenate([idxs_train,adapted_idxs_samples_reaching_specific_leaves[leaf_index]])
            position = leaves[int(leaf_index.split('_')[1])].get_infos()['position_relative_to_parent']
            tree.fit(data_concat,target_concat,indexes_train=idxs_all,current_depth=leaves[int(leaf_index.split('_')[1])].get_depth(),info_direction=position)
            tree.get_root().add_infos('parent',leaves[int(leaf_index.split('_')[1])].get_infos()['parent'])
            # replacing the leaf with the subtree 
            if position == 'left':
                leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_left_child(tree.get_root())
            else:
                leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_right_child(tree.get_root())
            #print('---')
        return np.concatenate([Xold,Xnew],axis=0),np.concatenate([yold,ynew])
    
    def print_node_rec(self,node,lines,nt=0):
        if node.is_leaf():
            lines.append("\t"*nt+f"leaf:{node}")
            lines.append("\t"*nt+f"{node.get_infos()}")
        else:
            lines.append("\t"*nt+f"{node}")
            lines.append("\t"*nt+f"{node.get_infos()}")
            lines.append("\t"*nt+f"left:")
            self.print_node_rec(node.get_left_child(),lines,nt+1)
            lines.append("\t"*nt+f"right:")
            self.print_node_rec(node.get_right_child(),lines,nt+1)
    def __str__(self):
        lines = ['root:']
        self.print_node_rec(self.get_root(),lines)
        return "\n".join(lines)

class NCM_Tree_Incremental_v1(NCM_Tree_Incremental_v0):
    def IGT(self,Xnew,ynew,Xold,yold):
        _classesnew = np.unique(ynew)
        for k in _classesnew:
            if k not in self._classes:
                self._classes = np.concatenate([self._classes,np.array([k])])
        leaves = []
        idxs_samples_reaching_specific_leaves = {}
        adapted_idxs_samples_reaching_specific_leaves = {}
        for i,x in enumerate(Xnew):
            leaf = self._propagate_to_leaf(x.reshape(1,-1), self.get_root(), metric='euclidean')
            if leaf not in leaves:
                idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                adapted_idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                leaves.append(leaf)
            idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i)
            adapted_idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i+len(Xold))

        for leaf_index,indexes_samples in idxs_samples_reaching_specific_leaves.items():

            idxs_train = leaves[int(leaf_index.split('_')[1])].get_infos()['idxs_train']

            data_concat = np.concatenate([Xold[idxs_train,:], Xnew[indexes_samples,:]], axis=0)
            assert data_concat.shape[0] == (Xold[idxs_train,:].shape[0] + Xnew[indexes_samples,:].shape[0])
            assert data_concat.shape[1] == Xold.shape[1] == Xnew.shape[1]

            target_concat = np.concatenate([yold[idxs_train], ynew[indexes_samples]])
            assert len(target_concat) == (len(yold[idxs_train]) + len(ynew[indexes_samples]))


            tree = NCM_Tree_Incremental_v1()

            idxs_all = np.concatenate([idxs_train,adapted_idxs_samples_reaching_specific_leaves[leaf_index]])
            position = leaves[int(leaf_index.split('_')[1])].get_infos()['position_relative_to_parent']
            tree.fit(data_concat,target_concat,indexes_train=idxs_all,current_depth=leaves[int(leaf_index.split('_')[1])].get_depth(),info_direction=position)
            tree.get_root().add_infos('parent',leaves[int(leaf_index.split('_')[1])].get_infos()['parent'])
            # replacing the leaf with the subtree 
            if position == 'left':
                leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_left_child(tree.get_root())
            else:
                leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_right_child(tree.get_root())
            #print('---')
        return np.concatenate([Xold,Xnew],axis=0),np.concatenate([yold,ynew])

            
class NCM_Tree_Incremental_v2(NCM_Tree_Incremental_v1):
    """
        Pour l'incrementation en classe, IGT va planter car sur le premier batch le modele est une racine et n'a pas de parent
        Ceci fait planter la fonction IGT sur la ligne : 
            position = leaves[int(leaf_index.split('_')[1])].get_infos()['position_relative_to_parent']
    """
    def __init__(self, max_depth=None, min_sample_split=2, max_features=None, min_sample_leaf=1, max_classes=None, *args, **kwargs):
        super().__init__(max_depth, min_sample_split, max_features, min_sample_leaf, max_classes, *args, **kwargs)
        self._confusions = None
        self._corresp_cm_label_idx = None
        self._sum_confusions = None

    def IGT(self,Xnew,ynew,Xold,yold, debug=False):
        _classesnew = np.unique(ynew)
        if debug:print(f"before igt, classes seen: {self._classes}")
        for k in _classesnew:
            if k not in self._classes:
                self._classes = np.concatenate([self._classes,np.array([k])])
        if debug:print(f"after igt, classes seen: {self._classes}")
        
        leaves = []
        idxs_samples_reaching_specific_leaves = {}
        adapted_idxs_samples_reaching_specific_leaves = {}
        for i,x in enumerate(Xnew):
            leaf = self._propagate_to_leaf(x.reshape(1,-1), self.get_root(), metric='euclidean')
            if leaf not in leaves:
                idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                adapted_idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                leaves.append(leaf)
            idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i)
            adapted_idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i+len(Xold))
        
        # on propage chaque exemple de Xnew et on regarde dans quelle feuille il tombe
        if debug:print(f"Number of leaves reached by the samples in Xnew: {len(leaves)}")
        if debug:print(leaves, idxs_samples_reaching_specific_leaves)
        if debug:print(f"toutes les observations de Xnew doivent etre tombees dans la feuille")
        if debug:print(f"nombre d'observations de Xnew: {len(Xnew)}, nb tombees dans la feuille 0: {idxs_samples_reaching_specific_leaves['leaf_0']}")
        
        # pour chaque feuille on construit un arbre, la racine de l'arbre construit remplace le noeud feuille
        for leaf_index,indexes_samples in idxs_samples_reaching_specific_leaves.items():

            idxs_train = leaves[int(leaf_index.split('_')[1])].get_infos()['idxs_train']
            if debug: print(idxs_train)
            if debug: print(len(idxs_train))

            data_concat = np.concatenate([Xold[idxs_train,:], Xnew[indexes_samples,:]], axis=0)
            assert data_concat.shape[0] == (Xold[idxs_train,:].shape[0] + Xnew[indexes_samples,:].shape[0])
            assert data_concat.shape[1] == Xold.shape[1] == Xnew.shape[1]

            target_concat = np.concatenate([yold[idxs_train], ynew[indexes_samples]])
            assert len(target_concat) == (len(yold[idxs_train]) + len(ynew[indexes_samples]))


            tree = NCM_Tree_Incremental_v2()
            
            idxs_all = np.concatenate([idxs_train,adapted_idxs_samples_reaching_specific_leaves[leaf_index]])
            infos = leaves[int(leaf_index.split('_')[1])].get_infos()
            if 'position_relative_to_parent' in infos:
                position = leaves[int(leaf_index.split('_')[1])].get_infos()['position_relative_to_parent']
            else:
                position = "None"
            tree.fit(data_concat,target_concat,indexes_train=idxs_all,current_depth=leaves[int(leaf_index.split('_')[1])].get_depth(),info_direction=position)
            if 'parent' in leaves[int(leaf_index.split('_')[1])].get_infos():
                tree.get_root().add_infos('parent',leaves[int(leaf_index.split('_')[1])].get_infos()['parent'])
                # replacing the leaf with the subtree 
                if position == 'left':
                    leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_left_child(tree.get_root())
                else:
                    leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_right_child(tree.get_root())
                #print('---')
            else:
                #ici on est dans le cas ou on est dans la racine
                self._root = tree.get_root()
        return np.concatenate([Xold,Xnew],axis=0),np.concatenate([yold,ynew])
    
    def fit_igtc(self, X, y=None, indexes_train = None,current_depth=None,info_direction=None,*args, **kwargs):
        #self._set_classes(y)
        self._set_max_features(X)
        root = self._init_root(X,y)
        if indexes_train is None:
            indexes_train = np.array([i for i in range(len(X))])
        if current_depth is None:
            current_depth = 0
        self._build_tree_igtc(root,X,y,indexes_train=indexes_train, current_depth=current_depth,info_direction=info_direction)
    

    def get_CH1_CH2(self,X,y,label_to_switch,idx_to_switch):
        '''
        First measure (CH1) is computed in a normal way.
        Second measure (CH2) is computed by switching specific (idxs_to_switch) samples labels to 'label_to_switch'
        '''
        #ch1
        ch1 = calinski(X,y)
        #ch2
        modified_y = np.copy(y)
        modified_y[idx_to_switch] = label_to_switch
        ch2 = calinski(X,modified_y)
        return ch1,ch2

    def get_strategy(self,ch1,ch2, centroids, wrong_sample, idx_correct_centroid):
        '''
        Returns AC or UC strategy
        '''
        if ch1 >= ch2:
            return 'UC'
        else:
            # d1 : distance between wrong sample and its centroid
            d1 = np.linalg.norm(wrong_sample-centroids[idx_correct_centroid])
            # d2 : distance between both centroids 0 and 1 (not on additional centroids)
            d2 = np.linalg.norm(centroids[0]-centroids[1]) 
            if d1<=d2:
                return 'UC'
            return 'AC'

    def choose_strategy_igtc(self,infos_split,X,y,idxs_left_wrong,idxs_right_wrong):
        # define the strategy to choose for each sample wrongly oriented
        # left
        left_strategies = {}
        for wsl in idxs_left_wrong:
            ch1,ch2 = self.get_CH1_CH2(X,y,infos_split['pair_labels'][0],wsl)
            left_strategies[wsl] = self.get_strategy(ch1,ch2,infos_split['centroids'][:2,:], X[wsl], 0)

        # right
        right_strategies = {}
        for wsr in idxs_right_wrong:
            ch1,ch2 = self.get_CH1_CH2(X,y,infos_split['pair_labels'][1],wsr)
            right_strategies[wsr] = self.get_strategy(ch1,ch2,infos_split['centroids'][:2,:], X[wsr], 1)
        return left_strategies,right_strategies
    
    def correction_igtc(self,strats, infos_split, X, y, label_centroid, other_centroid):
        '''
        strats parameter is a dictionary
        The keys of strats are the indexes in X of the samples that we have to handle
        label_centroid is 0 or 1 and will be used when centroids size will be higher than 2 
        '''
        #print(infos_split)
        #print(strats)
        cents = np.copy(infos_split['centroids'])
        if 'labels_ac' not in infos_split:
            labels_ac = list(infos_split['pair_labels'])
        else:
            labels_ac = infos_split['labels_ac']
        for k,v in strats.items():
            if v == 'AC':
                sample = np.copy(X[k]).reshape(1,cents.shape[1])
                cents = np.concatenate([cents,sample],axis=0)
                #print(cents.shape)
                labels_ac.append(label_centroid)
            else:
                # strategy UC
                if label_centroid == 0:
                    n_centroid = 'nX_left'
                else:
                    n_centroid = 'nX_right'
                if n_centroid not in infos_split:
                    nX = len(y[y==labels_ac[label_centroid]])
                else:
                    nX = infos_split[n_centroid]
                occ_confusion = self._confusions[labels_ac[label_centroid]][self._corresp_cm_label_idx[labels_ac[other_centroid]]]
                if occ_confusion > 0:
                    #w_ij = round(100*occ_confusion/self._sum_confusions)
                    w_ij = round(100*occ_confusion/self._confusions[labels_ac[label_centroid]].sum())
                else:
                    w_ij = 1
                cents[label_centroid] = (nX * cents[label_centroid] + w_ij * X[k])/(nX + w_ij)
                nX += w_ij
                infos_split[n_centroid] = nX 

                
        infos_split['centroids'] = cents
        infos_split['labels_ac'] = labels_ac
        return infos_split

    def handle_wrong_directions(self,node,infos_split,X,y,idxs_wrong_samples_left, idxs_wrong_samples_right):
        # select only X whose label is in centroids (filtre)
        # si je selectionne de cette maniere mes indices wrong left et right ne vont plus correspondre
        # je cree un vecteur rempli de 0 de la meme taille que y et j'y insere 1 quand une donnee est mal dirigee
        correspondances_left_idxs = np.zeros(len(y))
        correspondances_right_idxs = np.zeros(len(y))
        if idxs_wrong_samples_left is not None:
            for id in idxs_wrong_samples_left:
                correspondances_left_idxs[id] = 1
        if idxs_wrong_samples_right is not None:
            for id in idxs_wrong_samples_right:
                correspondances_right_idxs[id] = 1

        idxs_cents = np.isin(y,infos_split['pair_labels'])

        left_strats, right_strats = self.choose_strategy_igtc(
            infos_split, X[idxs_cents], y[idxs_cents], 
            np.where(correspondances_left_idxs[idxs_cents]==1)[0],
            np.where(correspondances_right_idxs[idxs_cents]==1)[0])
        if left_strats:
            infos_split = self.correction_igtc(left_strats, infos_split, X[idxs_cents], y[idxs_cents], 0, 1)
        if right_strats:
            infos_split = self.correction_igtc(right_strats, infos_split, X[idxs_cents], y[idxs_cents], 1, 0)
        node._infos_split = infos_split
        idxs_cent = self.get_nearest_centroid_label(node,X,infos_split['centroids'],'euclidean')
        left_idxs = idxs_cent == 0#comb[0]
        right_idxs = idxs_cent == 1#comb[1]
        measure = self._evaluate_split(y, y[left_idxs],y[right_idxs])
        if measure == (-inf):
            return None,None,False,None
        return left_idxs, right_idxs, True, infos_split
        

    def get_nearest_centroid_label(self,node,X,centroids,metric='euclidean'):
        idxs_nearest_cents =  node.get_id_nearest_centroid(X,centroids,metric)
        try:
            #print(idxs_nearest_cents)
            idxs = np.unique(idxs_nearest_cents)
            #if 4 in idxs or 6 in idxs:
                #print('debug')
            for id in idxs:
                    idxs_current_id = np.where(idxs_nearest_cents==id)[0]
                    for idcur in idxs_current_id:
                        if idxs_nearest_cents[idcur] not in [0,1]:
                            idxs_nearest_cents[idcur] = node._infos_split['labels_ac'][id]
            #print(idxs_nearest_cents)
            #print('..')
        except:
            #print(idxs_nearest_cents)
            #print('--')
            pass#print(f"_infos_split not in node, idxs centroids: {idxs}")
        return idxs_nearest_cents

    def _find_best_split_igtc(self,node,X,y):
        left,right,is_split,infos_split = self._find_best_split(node,X,y)
        if is_split:
            isAlright, idxs_wrong_samples_left, idxs_wrong_samples_right = self.check_directions(X,y,left,right,infos_split)
            if isAlright: 
                return left,right,is_split,infos_split
            else:
                left,right,is_split,infos_split = self.handle_wrong_directions(node,infos_split,X,y,idxs_wrong_samples_left, idxs_wrong_samples_right)
                return left,right,is_split,infos_split
    
    
    def check_directions(self,X,y,left,right,infos_split,*args,**kwargs):
        '''
        On teste les directions des observations. 
        Si,
            une ou plusieurs observations, dont la classe est celle des deux centroides,
            s'orientent dans la mauvaise direction (vers l'autre centroide),
        alors,
            on renvoie un tuple (
                boolean : True =  tout va bien, 
                np array : idxs of wrong X left, 
                np array : idxs of wrong X right
                )

        '''
        #print('=====> IN check_directions')
        #print(infos_split)
        #print(f"left centroid's label is {infos_split['pair_labels'][0]} and right is {infos_split['pair_labels'][1]}")
        #print(f"left:{y[left]}")
        #print(f"right:{y[right]}")
        #print(f"is there a sample going to left instead of right ? {infos_split['pair_labels'][1] in y[left]}")
        #print(f"is there a sample going to right instead of left ? {infos_split['pair_labels'][0] in y[right]}")
        idxs_wrong_left = None
        idxs_wrong_right = None
        if infos_split['pair_labels'][1] in y[left]:
            #print('wrong direction!')
            # on recupere les indices des observations qui sont allees a gauche au lieu d'aller a droite
            idxs_wrong_left = np.where(left==True)[0][np.where(y[left]==infos_split['pair_labels'][1])[0]]
            #print(left)
            #print(f"observations wrongly going to left: {idxs_wrong_left}")
        if infos_split['pair_labels'][0] in y[right]:
            #print('wrong direction!')
            # on recupere les indices des observations qui sont allees a droite au lieu d'aller a gauche
            idxs_wrong_right = np.where(right==True)[0][np.where(y[right]==infos_split['pair_labels'][0])[0]]
            #print(right)
            #print(f"observations wrongly going to right: {idxs_wrong_right}")
        if idxs_wrong_left is None and idxs_wrong_right is None:
            return (True,None,None)
        else:
            # TRUE POUR DEBUG REMETTRE A FALSE APRES DEBUG
            return (False,idxs_wrong_left,idxs_wrong_right)
        

    # add indexes of X everywhere ...
    def _build_tree_igtc(self, node:AbstractNode,X,y,current_depth=0,indexes_train=None,*args,**kwargs):
        '''
        find best split retourne left, right et is_split.
        Si is_split, 
            je dois verifier que les donnees ayant pour label celui d'un des deux centroides du noeud, partent dans les bonnes directions.
            je dois definir une strategie de resolution du probleme si il y a
            je resous le probleme

        '''
        if 'info_direction' in kwargs and current_depth != 0:
            node.add_infos('position_relative_to_parent',kwargs['info_direction'])
        if self.is_stopping_condition_reached(y,current_depth):
            self.prepare_leaf(node,current_depth,y,indexes_train=indexes_train)
        else:
            features_indexes = sorted(random.sample(range(X.shape[1]), self.max_features))
            # c'est ici que je fais la recherche et resolution d'eventuelles mauvaises directions en utilisant calinski
            left,right,is_split,infos_split = self._find_best_split_igtc(node,X[:,features_indexes],y)
            if is_split:
                self.prepare_node(node,current_depth, features_indexes, infos_split)
                self._build_tree_igtc(node.get_left_child(),X[left],y[left],current_depth+1,indexes_train[left],info_direction='left')
                self._build_tree_igtc(node.get_right_child(),X[right],y[right],current_depth+1,indexes_train[right],info_direction='right')
            else:
                # if cannot split then make a leaf
                self.prepare_leaf(node,current_depth,y,indexes_train=indexes_train)

    def igtc(self,Xnew,ynew,Xold,yold, debug=False):
        '''
        OJO!:
        * Vérifier que l'arbre connaît y avec son attribut self._classes, mettre à jour sinon.
        * Vérifier si activation d'un IGT.
        * Construire un arbre fit_igtc au lieu de fit (bien mettre la 
        profondeur actuelle pour qu'elle soit prise en compte)
        '''
        _classesnew = np.unique(ynew)
        if debug:print(f"before igt, classes seen: {self._classes}")
        for k in _classesnew:
            if k not in self._classes:
                self._classes = np.concatenate([self._classes,np.array([k])])
        if debug:print(f"after igt, classes seen: {self._classes}")
        
        leaves = []
        idxs_samples_reaching_specific_leaves = {}
        adapted_idxs_samples_reaching_specific_leaves = {}
        for i,x in enumerate(Xnew):
            leaf = self._propagate_to_leaf(x.reshape(1,-1), self.get_root(), metric='euclidean')
            if leaf not in leaves:
                idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                adapted_idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                leaves.append(leaf)
            idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i)
            adapted_idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i+len(Xold))
        
        # on propage chaque exemple de Xnew et on regarde dans quelle feuille il tombe
        if debug:print(f"Number of leaves reached by the samples in Xnew: {len(leaves)}")
        if debug:print(leaves, idxs_samples_reaching_specific_leaves)
        if debug:print(f"toutes les observations de Xnew doivent etre tombees dans la feuille")
        if debug:print(f"nombre d'observations de Xnew: {len(Xnew)}, nb tombees dans la feuille 0: {idxs_samples_reaching_specific_leaves['leaf_0']}")
        
        # pour chaque feuille on construit un arbre, la racine de l'arbre construit remplace le noeud feuille
        for leaf_index,indexes_samples in idxs_samples_reaching_specific_leaves.items():

            idxs_train = leaves[int(leaf_index.split('_')[1])].get_infos()['idxs_train']
            if debug: print(idxs_train)
            if debug: print(len(idxs_train))

            data_concat = np.concatenate([Xold[idxs_train,:], Xnew[indexes_samples,:]], axis=0)
            assert data_concat.shape[0] == (Xold[idxs_train,:].shape[0] + Xnew[indexes_samples,:].shape[0])
            assert data_concat.shape[1] == Xold.shape[1] == Xnew.shape[1]

            target_concat = np.concatenate([yold[idxs_train], ynew[indexes_samples]])
            assert len(target_concat) == (len(yold[idxs_train]) + len(ynew[indexes_samples]))


            tree = NCM_Tree_Incremental_v2()
            tree._confusions = self._confusions
            tree._corresp_cm_label_idx = self._corresp_cm_label_idx
            tree._sum_confusions = self._sum_confusions
            
            idxs_all = np.concatenate([idxs_train,adapted_idxs_samples_reaching_specific_leaves[leaf_index]])
            infos = leaves[int(leaf_index.split('_')[1])].get_infos()
            if 'position_relative_to_parent' in infos:
                position = leaves[int(leaf_index.split('_')[1])].get_infos()['position_relative_to_parent']
            else:
                position = "None"
            tree.fit_igtc(data_concat,target_concat,indexes_train=idxs_all,current_depth=leaves[int(leaf_index.split('_')[1])].get_depth(),info_direction=position)
            if 'parent' in leaves[int(leaf_index.split('_')[1])].get_infos():
                tree.get_root().add_infos('parent',leaves[int(leaf_index.split('_')[1])].get_infos()['parent'])
                # replacing the leaf with the subtree 
                if position == 'left':
                    leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_left_child(tree.get_root())
                else:
                    leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_right_child(tree.get_root())
                #print('---')
            else:
                #ici on est dans le cas ou on est dans la racine
                self._root = tree.get_root()
        return np.concatenate([Xold,Xnew],axis=0),np.concatenate([yold,ynew])

    def _get_direction(self, sample, node:NCM_Node_v0, metric='euclidean', *args, **kwargs):
        direction = None
        centroids = node.get_infos()['split']['centroids']
        id_nearest_cent = self.get_nearest_centroid_label(node,sample[:,node.get_infos()['features_indexes']],centroids,metric)[0]
        if id_nearest_cent == 0:
            direction = 'left'
        elif id_nearest_cent == 1:
            direction = 'right'
        if direction is None:
            raise ValueError(f"direction is not found")
        return direction
   
class NCM_Tree_Incremental_v3(NCM_Tree_Incremental_v2):
    def _build_tree(self, node:AbstractNode,X,y,current_depth=0,indexes_train=None,*args,**kwargs):
        if 'info_direction' in kwargs and current_depth != 0:
            node.add_infos('position_relative_to_parent',kwargs['info_direction'])
        if self.is_stopping_condition_reached(y,current_depth):
            self.prepare_leaf(node,current_depth,y,indexes_train=indexes_train)
        else:
            features_indexes = sorted(random.sample(range(X.shape[1]), self.max_features))
            left,right,is_split,infos_split = self._find_best_split(node,X[:,features_indexes],y)
            if is_split:
                self.prepare_node(node,current_depth, features_indexes, infos_split, data=X, target=y)
                self._build_tree(node.get_left_child(),X[left],y[left],current_depth+1,indexes_train[left],info_direction='left')
                self._build_tree(node.get_right_child(),X[right],y[right],current_depth+1,indexes_train[right],info_direction='right')
            else:
                # if cannot split then make a leaf
                self.prepare_leaf(node,current_depth,y,indexes_train=indexes_train)  
    
    def prepare_node(self,node:AbstractNode,depth,features_indexes,infos_split,*args,**kwargs):
        super().prepare_node(node,depth,features_indexes,infos_split)
        if 'data' in kwargs and 'target' in kwargs:
            X = kwargs['data']
            y = kwargs['target']
            covs = {}
            cents = {}
            # compute covariance matrix for each class and store it in covs, with the class label as key
            for label in np.unique(y):
                idxs_label = y == label
                covs[label] = np.cov(X[idxs_label], rowvar=False)
                cents[label] = np.mean(X[idxs_label],axis=0)
            node.add_infos('covs',covs)
            node.add_infos('cents',cents)


        node.set_left_child(NCM_Node_v0())
        node.set_right_child(NCM_Node_v0())
        node.get_left_child().add_infos('parent',node)
        node.get_right_child().add_infos('parent',node)

    def prepare_leaf(self,node:AbstractNode,depth,y,*args,**kwargs):
        super().prepare_leaf(node,depth,y, *args, **kwargs)
        self._leaves.append(node)
          
class NCM_Tree_Incremental_v4(NCM_Tree_Incremental_v3):
    def IGT(self,Xnew,ynew,Xold,yold, debug=False):
        _classesnew = np.unique(ynew)
        if debug:print(f"before igt, classes seen: {self._classes}")
        for k in _classesnew:
            if k not in self._classes:
                self._classes = np.concatenate([self._classes,np.array([k])])
        if debug:print(f"after igt, classes seen: {self._classes}")
        
        leaves = []
        idxs_samples_reaching_specific_leaves = {}
        #adapted_idxs_samples_reaching_specific_leaves = {}
        for i,x in enumerate(Xnew):
            leaf = self._propagate_to_leaf(x.reshape(1,-1), self.get_root(), metric='euclidean')
            if leaf not in leaves:
                idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                #adapted_idxs_samples_reaching_specific_leaves[f"leaf_{len(leaves)}"] = []
                leaves.append(leaf)
            idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i)
            #adapted_idxs_samples_reaching_specific_leaves[f"leaf_{leaves.index(leaf)}"].append(i+len(Xold))
        
        # on propage chaque exemple de Xnew et on regarde dans quelle feuille il tombe
        if debug:print(f"Number of leaves reached by the samples in Xnew: {len(leaves)}")
        if debug:print(leaves, idxs_samples_reaching_specific_leaves)
        if debug:print(f"toutes les observations de Xnew doivent etre tombees dans la feuille")
        if debug:print(f"nombre d'observations de Xnew: {len(Xnew)}, nb tombees dans la feuille 0: {idxs_samples_reaching_specific_leaves['leaf_0']}")
        
        # pour chaque feuille on construit un arbre, la racine de l'arbre construit remplace le noeud feuille
        for leaf_index,indexes_samples in idxs_samples_reaching_specific_leaves.items():

            idxs_train = leaves[int(leaf_index.split('_')[1])].get_infos()['idxs_train']
            if debug: print(idxs_train)
            if debug: print(len(idxs_train))
            # Generating Xold and yold data


            classes, counts = leaves[int(leaf_index.split('_')[1])].get_distribution()
            dict_samples = {}
            for classe, count in zip(classes, counts):
                dict_samples[classe] = count
            synthetic_X = []
            synthetic_y = []
            # we create a new dataset
            for k,v in dict_samples.items():

                parent = leaves[int(leaf_index.split('_')[1])]._infos_node['parent'] # getting the parent of the leaf
                while True:
                    if k in parent._infos_node['cents'].keys():
                        # get the centroid and the cov matrix of the current label k in the parent node
                        cent_k = parent._infos_node['cents'][k]
                        cov_k = parent._infos_node['covs'][k]
                        # generating the new samples with multivariate normal distribution using the function multivariate_normal from scipy
                        #mn = multivariate_normal(mean=cent_k, cov=cov_k)
                        #new_samples = mn.rvs(size=v)
                        # check that cov_k is 2 dimensional and square
                        if len(cov_k.shape) == 2:
                            break
                        else:
                            parent = parent._infos_node['parent']
                    else:
                        parent = parent._infos_node['parent']

                new_samples = multivariate_normal(mean=cent_k, cov=cov_k, size=v)
                # adding the new samples to the dataset
                synthetic_X.extend(new_samples)
                synthetic_y.extend([k for i in range(v)])
            Xold = np.array(synthetic_X)
            yold = np.array(synthetic_y)





            data_concat = np.concatenate([Xold, Xnew[indexes_samples,:]], axis=0)
            assert data_concat.shape[0] == (Xold.shape[0] + Xnew[indexes_samples,:].shape[0])
            assert data_concat.shape[1] == Xold.shape[1] == Xnew.shape[1]

            target_concat = np.concatenate([yold, ynew[indexes_samples]])
            assert len(target_concat) == (len(yold) + len(ynew[indexes_samples]))


            tree = NCM_Tree_Incremental_v4()
            tree._leaves = []
            adapted_idxs_samples_reaching_specific_leaves = np.array(range(len(yold)))
            idxs_all = None#np.concatenate([idxs_train, adapted_idxs_samples_reaching_specific_leaves])
            infos = leaves[int(leaf_index.split('_')[1])].get_infos()
            if 'position_relative_to_parent' in infos:
                position = leaves[int(leaf_index.split('_')[1])].get_infos()['position_relative_to_parent']
            else:
                position = "None"
            tree.fit(data_concat,target_concat,indexes_train=idxs_all,current_depth=leaves[int(leaf_index.split('_')[1])].get_depth(),info_direction=position)
            if 'parent' in leaves[int(leaf_index.split('_')[1])].get_infos():
                tree.get_root().add_infos('parent',leaves[int(leaf_index.split('_')[1])].get_infos()['parent'])
                # replacing the leaf with the subtree 
                if position == 'left':
                    leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_left_child(tree.get_root())
                else:
                    leaves[int(leaf_index.split('_')[1])].get_infos()['parent'].set_right_child(tree.get_root())
                #print('---')
            else:
                #ici on est dans le cas ou on est dans la racine
                self._root = tree.get_root()
        return np.concatenate([Xold,Xnew],axis=0),np.concatenate([yold,ynew])

    def _build_tree(self, node:AbstractNode,X,y,current_depth=0,indexes_train=None,*args,**kwargs):
        if 'info_direction' in kwargs and current_depth != 0:
            node.add_infos('position_relative_to_parent',kwargs['info_direction'])
        if self.is_stopping_condition_reached(y,current_depth):
            self.prepare_leaf(node,current_depth,y,indexes_train=indexes_train)
        else:
            features_indexes = sorted(random.sample(range(X.shape[1]), self.max_features))
            left,right,is_split,infos_split = self._find_best_split(node,X[:,features_indexes],y)
            if is_split:
                self.prepare_node(node,current_depth, features_indexes, infos_split, data=X, target=y)
                self._build_tree(node.get_left_child(),X[left],y[left],current_depth+1,None,info_direction='left')
                self._build_tree(node.get_right_child(),X[right],y[right],current_depth+1,None,info_direction='right')
            else:
                # if cannot split then make a leaf
                self.prepare_leaf(node,current_depth,y,indexes_train=indexes_train)  
######################################################################################################################################################
################################################################# FOREST #############################################################################
######################################################################################################################################################



class AbstractForest(ABC):
    def __init__(self,n_trees=10):
        self.n_trees = n_trees
        self.trees = []
    @abstractmethod
    def fit(self,X,y,*args,**kwargs):pass
    @abstractmethod
    def predict(self,X,*args,**kwargs):pass
    @abstractmethod
    def predict_proba(self,X,*args,**kwargs):pass
    @abstractmethod
    def score(self,X,y,*args,**kwargs):pass

class ClassicalForest(AbstractForest):
    def __init__(self,n_trees=10,max_depth=None,min_sample_split=2,max_features=None,min_sample_leaf=1):
        super().__init__(n_trees)
        self.max_depth = max_depth
        self.min_sample_split = min_sample_split
        self.min_sample_leaf = min_sample_leaf
        self.max_features = max_features
        self.bootstrap_idxs = None
    
    def predict_proba(self,X, metric='euclidean', *args, **kwargs):
        '''
        c'est un Phi de B si kwargs['mode']='counts', Phi de A si 'probas'
        '''
        #print([t.predict_proba(X,mode='counts',metric=metric) for t in self.trees])
        if 'mode' in kwargs:
            counts = np.array([t.predict_proba(X,mode=kwargs['mode'],metric=metric) for t in self.trees])
        else:
            counts = np.array([t.predict_proba(X,mode='counts',metric=metric) for t in self.trees])
        #sum_counts_cols = counts.sum(axis=1)
        sum_counts_rows = counts.sum(axis=0)
        #print(counts)
        #print(sum_counts_cols)
        #print(sum_counts_rows)
        #print(sum_counts_rows/sum_counts_rows.sum())
        return sum_counts_rows/sum_counts_rows.sum()
    
    def predict(self, X, mode='majority_vote',*args, **kwargs):
        if 'phi' in kwargs:
            preds = np.array([t.predict(X, phi=kwargs['phi']) for t in self.trees])
        else:
            preds = np.array([t.predict(X) for t in self.trees])
        res = []
        for col in range(preds.shape[1]):
            res.append(Counter(preds[:,col]).most_common(1)[0][0])
        return np.array(res)
    
    def score(self, X, y, *args, **kwargs):
        preds = self.predict(X)
        return np.mean(preds == y)

class NCM_Forest_v0(ClassicalForest):
    def _init_trees(self,max_depth,min_sample_split,max_features,min_sample_leaf,max_classes):
        self.trees = [
            NCM_Tree_v0(
                max_depth=max_depth,min_sample_split=min_sample_split,
                max_features=max_features,min_sample_leaf=min_sample_leaf,max_classes=max_classes
            ) for i in range(self.n_trees)] 
    def __init__(self,n_trees=10,max_depth=None,min_sample_split=2,max_features=None,min_sample_leaf=1,max_classes=None):
        super().__init__(n_trees,max_depth,min_sample_split,max_features,min_sample_leaf)
        self.max_classes = max_classes

    def fit(self, X, y, *args, **kwargs):
        self._init_trees(self.max_depth,self.min_sample_split,self.max_features,self.min_sample_leaf,self.max_classes)
        for tree in self.trees:
            tree._set_classes(y)
        idxs = [i for i in range(len(X))]
        for tree in self.trees:
            bootstrap_idxs = np.random.choice(idxs, len(idxs), replace=True, p=None)
            self.bootstrap_idxs = bootstrap_idxs
            tree.fit(X[bootstrap_idxs,:],y[bootstrap_idxs])
        
class NCM_Forest_Incremental_v0(NCM_Forest_v0):
    '''
    This version increment multiple samples at the time
    '''
    def _init_trees(self,max_depth,min_sample_split,max_features,min_sample_leaf,max_classes):
        self.trees = [
            NCM_Tree_Incremental_v0(
                max_depth=max_depth,min_sample_split=min_sample_split,
                max_features=max_features,min_sample_leaf=min_sample_leaf,max_classes=max_classes
            ) for i in range(self.n_trees)] 
    def IGT(self,Xnew,ynew,Xold,yold):
        for t in self.trees:
            Xaugmented,yaugmented = t.IGT(Xnew,ynew,Xold,yold)           
        return Xaugmented,yaugmented

class NCM_Forest_Incremental_v1(NCM_Forest_Incremental_v0):
    '''
    This version increment one sample at the time
    DEPRECATED : tres mauvaises perfs
    TODO : Only bad trees in further version
    '''
    def IGT(self,Xnew,ynew,Xold,yold):
        olddata = np.copy(Xold)
        oldtarget = np.copy(yold)
        for i,x in enumerate(Xnew):
            for t in self.trees:
                Xaugmented,yaugmented = t.IGT(x.reshape(1,-1),np.array([ynew[i]]),olddata,oldtarget)   
            olddata = np.copy(Xaugmented)
            oldtarget = np.copy(yaugmented)
        return Xaugmented,yaugmented

class NCM_Forest_Incremental_v2(NCM_Forest_Incremental_v0):
    '''
    This version increment one sample at the time only for bad trees
    '''
    def IGT(self,Xnew,ynew,Xold,yold):
        olddata = np.copy(Xold)
        oldtarget = np.copy(yold)
        Xaugmented = np.copy(Xold)
        yaugmented = np.copy(yold)
        for i,x in enumerate(Xnew):
            for t in self.trees:
                pred = t.predict(x.reshape(1,-1))[0]
                if pred != ynew[i]:
                    Xaugmented,yaugmented = t.IGT(x.reshape(1,-1),np.array([ynew[i]]),olddata,oldtarget)   
            olddata = np.copy(Xaugmented)
            oldtarget = np.copy(yaugmented)
        return Xaugmented,yaugmented


class NCM_Forest_Incremental_v3(NCM_Forest_Incremental_v2):
    '''
    This version increment one sample at the time only for bad trees
    '''
    def _init_trees(self,max_depth,min_sample_split,max_features,min_sample_leaf,max_classes):
        self.trees = [
            NCM_Tree_Incremental_v1(
                max_depth=max_depth,min_sample_split=min_sample_split,
                max_features=max_features,min_sample_leaf=min_sample_leaf,max_classes=max_classes
            ) for i in range(self.n_trees)] 

class NCM_Forest_Incremental_v4(NCM_Forest_Incremental_v3):
    '''
    This version uses NCM_Tree_Incremental_v2 for class incremental purpose
    '''
    def _init_trees(self,max_depth,min_sample_split,max_features,min_sample_leaf,max_classes):
        self.trees = [
            NCM_Tree_Incremental_v2(
                max_depth=max_depth,min_sample_split=min_sample_split,
                max_features=max_features,min_sample_leaf=min_sample_leaf,max_classes=max_classes
            ) for i in range(self.n_trees)] 
    
    '''
    This version increment one sample at the time only for bad trees
    Applies Calinski 
    '''
    def IGTC(self,Xnew,ynew,Xold,yold):
        olddata = np.copy(Xold)
        oldtarget = np.copy(yold)
        Xaugmented = np.copy(Xold)
        yaugmented = np.copy(yold)
        for i,x in enumerate(Xnew):
            for t in self.trees:
                pred = t.predict(x.reshape(1,-1))[0]
                if pred != ynew[i]:
                    Xaugmented,yaugmented = t.igtc(x.reshape(1,-1),np.array([ynew[i]]),olddata,oldtarget)   
            olddata = np.copy(Xaugmented)
            oldtarget = np.copy(yaugmented)
        return Xaugmented,yaugmented
    
    def save_confusions(self,Xtest,ytest):
        '''
        save confusions on each tree
        '''
        preds = self.predict(Xtest)
        confusions = confusion_matrix(ytest,preds)
        confs_dict = {}
        correspondance_confusion_matrix_label_idx = {}
        for idx,label in enumerate(np.unique(ytest)):
            confs_dict[label] = confusions[idx,:]
            correspondance_confusion_matrix_label_idx[label] = idx
        for t in self.trees:
            t._confusions = confs_dict
            t._sum_confusions = np.sum(confusions)
            t._corresp_cm_label_idx = correspondance_confusion_matrix_label_idx
        print(confusions)
        
class NCM_Forest_Incremental_v5(NCM_Forest_Incremental_v4):
    '''
    This version uses NCM_Tree_Incremental_v2 for class incremental purpose
    '''
    def _init_trees(self,max_depth,min_sample_split,max_features,min_sample_leaf,max_classes):
        self.trees = [
            NCM_Tree_Incremental_v3(
                max_depth=max_depth,min_sample_split=min_sample_split,
                max_features=max_features,min_sample_leaf=min_sample_leaf,max_classes=max_classes
            ) for i in range(self.n_trees)] 
    
    def fit(self, X, y, *args, **kwargs):
        self._init_trees(self.max_depth,self.min_sample_split,self.max_features,self.min_sample_leaf,self.max_classes)
        for tree in self.trees:
            tree._leaves = []
            tree._set_classes(y)
        idxs = [i for i in range(len(X))]
        for tree in tqdm(self.trees, 'fitting trees'):
            bootstrap_idxs = np.random.choice(idxs, len(idxs), replace=True, p=None)
            self.bootstrap_idxs = bootstrap_idxs
            tree.fit(X[bootstrap_idxs,:],y[bootstrap_idxs])
    
    # this function generates a new dataset according to a dictionary given in parameters
    # the dictionary is of the form {label: number of samples to generate}
    def generate_dataset(self,dict_samples, *args, **kwargs):
        # first, we randomly select one of the trees
        tree = self.trees[np.random.randint(0,len(self.trees))]
        # then, for each leaf of the tree, we gather in a dictionary the number of samples per each class
        # the key of the dictionary is the address in memory of the leaf
        # the value is a dictionary of the form {label: number of samples}
        dict_labels_per_leaf = {}
        for leaf in tree._leaves:
            classes, counts = leaf.get_distribution()
            dict_labels_per_leaf[id(leaf)] = {}
            for label, count in zip(classes, counts):
                dict_labels_per_leaf[id(leaf)][label] = count
        synthetic_X = []
        synthetic_y = []
        # we create a new dataset
        for k,v in dict_samples.items():
            # preparation of the probabilites
            # getting the number of samples of the class k for each leaf
            probs = []
            for key_leaf in dict_labels_per_leaf.keys():
                if k in dict_labels_per_leaf[key_leaf].keys():
                    probs.append(dict_labels_per_leaf[key_leaf][k])
                else:
                    probs.append(0)
            # normalizing the probabilities so the sum equals 1
            probs = np.array(probs)
            probs = probs / np.sum(probs)
            new_samples = []
            for i in tqdm(range(v), f"generating samples for class {k}"):
                # we randomly select the leaf according to the probabilities
                winner_leaf_id = np.random.choice(list(dict_labels_per_leaf.keys()), 1, p=probs)[0]
                # selecting the leaf according to the winner_leaf_id
                winner_leaf = [l for l in tree._leaves if id(l) == winner_leaf_id][0]
                parent = winner_leaf._infos_node['parent'] # getting the parent of the leaf
                while True:
                    # get the centroid and the cov matrix of the current label k in the parent node
                    cent_k = parent._infos_node['cents'][k]
                    cov_k = parent._infos_node['covs'][k]
                    # generating the new samples with multivariate normal distribution using the function multivariate_normal from scipy
                    #mn = multivariate_normal(mean=cent_k, cov=cov_k)
                    #new_samples = mn.rvs(size=v)
                    # check that cov_k is 2 dimensional and square
                    if len(cov_k.shape) == 2:
                        break
                    else:
                        parent = parent._infos_node['parent']

                new_samples.append(multivariate_normal(mean=cent_k, cov=cov_k, size=1)[0])
                # adding the new samples to the dataset
            synthetic_X.extend(new_samples)
            synthetic_y.extend([k for i in range(v)])
        return np.array(synthetic_X), np.array(synthetic_y)
        
class NCM_Forest_Incremental_v6(NCM_Forest_Incremental_v5):

    def _init_trees(self,max_depth,min_sample_split,max_features,min_sample_leaf,max_classes):
        self.trees = [
            NCM_Tree_Incremental_v4(
                max_depth=max_depth,min_sample_split=min_sample_split,
                max_features=max_features,min_sample_leaf=min_sample_leaf,max_classes=max_classes
            ) for i in range(self.n_trees)]
